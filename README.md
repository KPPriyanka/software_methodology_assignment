# README #

CSCE553-001-201820 PRINCIPLES OF SOFTWARE METHODOLOGY

A#4 - GET STARTED WITH GIT
I followed the steps suggested to get started with Git. I am very happy that I now have a good idea in basics of using Git as a group by doing this assignment. I had created a free account on Bitbucket.org and uploaded my eclipse project to it.
My Bitbucket project URL: https://bitbucket.org/KPPriyanka/software_methodology_assignment
I created a file “.gitignore” in the top level of my project directory and copied the content of the link  as given in the assignment instructions.
I made my code public to share it with the class. I looked into the code of others in the class to coordinate with them and make changes and commit into Git. While doing this, I felt good for having the opportunity to walk through others’ code and understand how better and in different ways the same functionality could be written in terms of efficiency. I could also suggest changes to their code with my ideas of betterment to their existing code. While working on Git, there were situations when I did few changes to a code of a person but within I could push it to the person’s repository, I got to know through the pull operation that the particular change was already committed by another person working on that repository. It was a good learning.

The projects that I considered and found to have sensible changes to be made are:
1.	Shivanjali Khare - https://bitbucket.org/shivanjalikhare/csce553_a-4
2.	Ashish Ippili - https://bitbucket.org/aashish61/csce553-aashish

The suggestions that I received for my public repository and the changes made to my project were by the following people.Shivanjali Khare and Ashish Ippili gave me few valuable suggestions that were needed to be implemented to make the code function much better.
