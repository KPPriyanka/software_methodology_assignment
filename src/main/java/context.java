//context class

public class context 
{
	public strategy strat;	
	public context(strategy strat) 
	{
		this.strat = strat;
	}
	public void executestrategy(String key) 
	{
		strat.doOperation(key);
	}
	
	public void displayoutput() {
		strat.print();
	}
}
