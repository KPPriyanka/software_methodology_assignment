
//To find the frequency of words in the input text file
import java.util.*;

public class frequency implements strategy {
	Map<String, Integer> wordCount;
	String words[] = null;
	String line = null;
	String filecontent;

	// constructor
	frequency() {
		wordCount = new HashMap<String, Integer>();
	}

	// splitting the words
	public String[] splitcontent(String filecontent) {
		try {
			if (filecontent != null) {
				line = filecontent.toLowerCase();
				words = line.split("\\s+"); // Splits the words with "space" as an delimeter
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return words;
	}

	// finding frequency
	public void calculatefrequency(String[] words) {
		try {

			// Creates an Hash Map for storing the words and its count

			for (String read : words) {
				Integer freq = wordCount.get(read);
				wordCount.put(read, (freq == null) ? 1 : freq++); // For Each word the count will be incremented in the
																	// Hashmap
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	public void doOperation(String key) {
		filecontent = key;
		splitcontent(filecontent);
		calculatefrequency(words);
	}

	public void print() {

		System.out.println(wordCount.size() + " distinct words:"); // Prints the Number of Distinct words found in the
																	// files read
		System.out.println(wordCount);
		// TODO Auto-generated method stub

	}
}
