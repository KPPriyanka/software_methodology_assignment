//search for a word in the input text file

public class grep implements strategy {
	String grepword;
	String content;

	// constructor
	grep(String word) {
		grepword = word;
	}

	public void doOperation(String key) {
		try {
			// checks if the word exists in each line and prints
			String[] words = key.split("\\s");
			for (String w : words) {
				if (w.equalsIgnoreCase(grepword)) {
					System.out.print(key);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void print() {
		// TODO Auto-generated method stub

	}
}
