import java.io.*;
import java.util.StringTokenizer;

public class strategypattern {
	public static void main(String[] args) throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(System.in));
		String operation = "";
		String filename = "";
		String word = "";
		while (true) {
			System.out.print(">");

			StringTokenizer tokens = new StringTokenizer(br.readLine());

			if (tokens.countTokens() == 2) {
				operation = tokens.nextToken();
				filename = tokens.nextToken();

			} else if (tokens.countTokens() == 3) {
				operation = tokens.nextToken();
				word = tokens.nextToken();
				filename = tokens.nextToken();
			} else {

			}

		try {
			
			// executing the operation based on the input command
			if (operation.equalsIgnoreCase("wc")) {
				context cont = new context(new wordcount());
				readfile(filename, cont, word);
				cont.displayoutput();
			}
			if (operation.equalsIgnoreCase("grep")) {
				context cont = new context(new grep(word));// to avoid unnecessary parameter word in frequency and wc
															// func
				readfile(filename, cont, word);
			}
			if (operation.equalsIgnoreCase("freq")) {
				context cont = new context(new frequency());
				readfile(filename, cont, word);
				cont.displayoutput();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	}

	public static void readfile(String filename, context cont, String word) {
		// changed the file addressing system to make it multi platform

		String OS = System.getProperty("os.name").toLowerCase();
		String pathName = "";
		if (OS.indexOf("win") >= 0) {
			pathName = "/Assignment-3/src/main/resources";
		} else {
			pathName = "src/main/resources/";
		}
		// reading the input text file and storing into key
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(pathName + filename + ".txt"));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				cont.executestrategy(line);
			}
			bufferedReader.close();
		} catch (IOException ex) {
			System.out.println("Unable to open file ' " + filename + " ' ");
		}
	}
}
