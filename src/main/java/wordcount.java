//find word count of the input text file

public class wordcount implements strategy {

	private int charCount;
	private int wordCount;
	private int lineCount;
	private String data;

	// constructor
	wordcount() {
		charCount = 0;
		wordCount = 0;
		lineCount = 0;

		// TODO Auto-generated constructor stub
	}

	// finding number of lines in the text file
	// changed line count code
	public void linecount(String data) {
		try {
			if (data != null && !data.isEmpty()) {
				lineCount += data.split("\n").length;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getCharCount() {
		return charCount;
	}

	public int getWordCount() {
		return wordCount;
	}

	public int getLineCount() {
		return lineCount;
	}

	// finding number of words in the text file
	// changed code by removing special characters
	public void wordcountt(String data) {
		try {
			if (data != null) {
				String[] words = data.replaceAll("[\\s\\n\\r.?,;/0-9/]", " ").split(" ");
				for (String w : words) {
					if (!(w.isEmpty())) {
						wordCount++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// finding number of characters in the text file
	// changed
	public void charcount(String data) {
		try {
			if (data != null) {
				charCount += data.replaceAll("[\\s,?.;'/0-9/]", "").length(); // charcount by finding length of each
																				// word
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doOperation(String key) {
		data = key;
		linecount(data);
		wordcountt(data);
		charcount(data);
	}

	public void print() {
		System.out.println("Number Of Chars In A File : " + charCount);
		System.out.println("Number Of Words In A File : " + wordCount);
		System.out.println("Number Of Lines In A File : " + lineCount);
	}
}