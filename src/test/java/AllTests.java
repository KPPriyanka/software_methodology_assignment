import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ frequencytest.class, greptest.class, strategypatterntest.class, wordcounttest.class })
public class AllTests {

}
