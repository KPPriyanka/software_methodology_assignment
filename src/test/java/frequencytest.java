//to test the functionality of calculatefrequency() method using Junit Parameterized test
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.After;
import org.junit.Before;

@RunWith(Parameterized.class)
public class frequencytest {
	public static frequency freq;
	HashMap<String, Integer> expectedresult = new HashMap<String, Integer>();
	//private String inputdata;
	private String word; //change
	private Integer frequency;
	
	@Before
	public void init() {
		freq = new frequency();//object 'freq' created for frequency
	}
	
	//frequencytest constructor
	public frequencytest(String data, Integer number) {
		//this.inputdata = data;
		this.word = data; //change
		this.frequency = number;
	}
	
	//Junit Parameterized Test
	@Parameterized.Parameters
	public static Collection frequencycollection() {
		return Arrays.asList(new Object[][] {
			{"software", 2},{"methodology",1},{"is",1},{"merely",1},{"about",1},{"the",1},{"entire",1},{"lifecycle",1}
		});
	}
	
	@Test
	public void test() {
		//testing frequency of words result
		String[] input = freq.splitcontent("Software Methodology is merely about the entire software lifecycle");
		freq.calculatefrequency(input);
		//assertEquals(2,(int)freq.wordCount.get("software"));//checks if computed frequency for "software" is 2
		//assertEquals(null,freq.wordCount.get("Harry"));//checks if key "harry" exists
		assertEquals((int)frequency,(int)freq.wordCount.get(word)); //change
		assertEquals(true,freq.wordCount.containsKey("software"));//checks if freq map contains key "software"
		assertEquals(false,freq.wordCount.containsKey("Sejal"));//checks if freq map contains key "Sejal"
	}
	
	@After
	public void end() {
		freq = null;//object 'freq' destroyed
	}
}
