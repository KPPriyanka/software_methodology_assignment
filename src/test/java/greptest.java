//to test the search functionality using Junit Parameterized Test
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runners.Parameterized;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Parameterized.class)
public class greptest {
	private String expectedresult;
	private String searchword;
	public static grep grepobj;
	private ByteArrayOutputStream outContent;

	@Before
	public void init() {
		grepobj = new grep(searchword);//object 'grepobj' created for grep
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	}
	
	//greptest constructor
	public greptest(String expectedresult, String searchword) {
		this.expectedresult = expectedresult;
		this.searchword = searchword;
	}
	
	//Junit Parameterized Test
	@Parameterized.Parameters
	public static Collection searchwordsearch() {
		return Arrays.asList(new Object[][]{
			{ "Alice in Wonderland", "Wonderland" },
			{ "Burger King or Subway or McD", "Subway" },
			{"I have enrolled for 2 courseworks","2"},
			{"I am @ pizza hut","@"}
		});
	}
	
	@Test
	public void test() throws Exception {
		grepobj.doOperation(expectedresult);
		assertEquals(expectedresult, outContent.toString());//checks if computed search result matches with the expected result	
	}
	
	@After
	public void end()
	{
		grepobj = null;// object 'grepobj' destroyed
	}

}
