
//to test the functionality of linecount(), wordcount() and charcount() methods using Junit Parameterized test
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class wordcounttest extends Object {
	private String inputdata;
	public static wordcount counter;
	private int expectedlinecount;
	private int expectedwordcount;
	private int expectedcharcount;

	@Before
	public void init() {
		counter = new wordcount();// object 'counter' created for wordcount
	}

	// wordcounttest constructor
	public wordcounttest(String text, Integer expectedl, Integer expectedC, Integer expectedw) {
		this.inputdata = text;
		this.expectedlinecount = expectedl;
		this.expectedcharcount = expectedC;
		this.expectedwordcount = expectedw;
	}

	// Junit Parameterized Test
	// modified code check empty file
	@Parameterized.Parameters
	public static Collection LCount() {
		return Arrays.asList(new Object[][] {
				// {"There were 2 fruits placed @ the seminar hall",1,37,7}
				{ "Testing\r\n" + "line two\r", 2, 14, 3 }, { "", 0, 0, 0 }, { null, 0, 0, 0 },
				{ "There are many variations of passages of Lorem Ipsum available,\n"
						+ " but the majority have suffered alteration in some form,\n" + " by injected humour, \n"
						+ " or randomised words which don't look even slightly believable. \n"
						+ " If you are going to use a passage of Lorem Ipsum,\n"
						+ "  you need to be sure there isn't anything embarrassing hidden in the middle of text.\n"
						+ "  All the Lorem Ipsum generators on the \n"
						+ " Internet tend to repeat predefined chunks as necessary, \n"
						+ " making this the first true generator on the Internet.\n"
						+ "  It uses a dictionary of over 200 Latin words,\n"
						+ " combined with a handful of model sentence structures, \n"
						+ " to generate Lorem Ipsum which looks reasonable.\n"
						+ "  The generated Lorem Ipsum is therefore always free from repetition,\n"
						+ " injected humour, or non-characteristic words etc.", 14, 611, 120 }, });
	}

	// testing word count of the input
	@Test
	public void testwc() {
		counter.wordcountt(inputdata);
		assertEquals(expectedwordcount, counter.getWordCount());// checks if computed word count matches the expected
																// word count result
	}

	// testing line count of the input
	@Test
	public void testlc() {
		counter.linecount(inputdata);
		assertEquals(expectedlinecount, counter.getLineCount());// checks if computed line count matches the expected
																// line count result
	}

	// testing character count of the input
	@Test
	public void testcc() {
		counter.charcount(inputdata);
		assertEquals(expectedcharcount, counter.getCharCount());// checks if computed character count matches the
																// expected character count result
	}

	@After
	public void end() {
		counter = null;// object 'counter' destroyed
	}
}